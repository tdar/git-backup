import unittest
import tasks
import os
import sys
import shutil
import logging
import hashlib
import filecmp
import configparser

from tasks import resolve
from tasks import makedirs

logging.basicConfig(stream=sys.stdout, )
logger = logging.getLogger(__name__)
logger.setLevel('INFO')

PWD = os.path.dirname(os.path.realpath(__file__))
RESOURCE_DIR = os.path.normpath(os.path.join(PWD, './resources'))
TARGET_DIR = os.path.normpath(os.path.join(PWD, '../target'))
SANDBOX_ROOT_DIR = os.path.normpath(os.path.join(TARGET_DIR, 'sandbox'))
FAKE_S3_DIR = resolve(TARGET_DIR, 's3')

# override the config_file_path so we don't wipe the real config file during testing
test_config_file_path = resolve(TARGET_DIR, 'config_test.ini')
tasks.DEFAULT_CONFIG_FILE_PATH = test_config_file_path


def sha(path):
    """ return sha256 of specified file. Only works with text files (for now)!
    :param path: path to file
    :return: string containint sha
    """
    crc = hashlib.sha256()
    with open(path, 'r') as f:
        for line in f:
            crc.update(line.encode())
    return crc.digest()


def count_lines(file):
    with open(file, 'r') as f:
        num_lines = sum(1 for line in f)
    return num_lines


def file_to_list(file):
    with open(file, 'r') as f:
        list_of_lines = [line.rstrip() for line in f.readlines()]
    return list_of_lines


def _fake_method(methodName):
    return lambda *args, **kwargs: logger.info('**intercepted** %s(%s, %s)', methodName, str(args), str(kwargs))


def _list_files(rootdir):
    return [resolve(root, file) for root, dirs, files in os.walk(rootdir) for file in files]


def _mock_put_archive(local_file, s3url, dry_run=False):
    remote_file = resolve(FAKE_S3_DIR, s3url.replace('s3://', ''))
    makedirs(os.path.dirname(remote_file))
    if not remote_file.startswith(FAKE_S3_DIR):
        sys.exit('sanity check failed - remote file must be inside %s' % (FAKE_S3_DIR))

    shutil.copyfile(local_file, remote_file)


def _mock_get_archive(s3url, local_file, dry_run=False):
    remote_file = resolve(FAKE_S3_DIR, s3url.replace('s3://', ''))
    shutil.copyfile(remote_file, local_file)


def _mock_get_backup_files(backup_name):
    files = []
    remote_path = resolve(FAKE_S3_DIR, 'tdar', backup_name)
    print("get backup files:  remote_path:%s", remote_path)
    for file in _list_files(remote_path):
        print(os.path.relpath(file, FAKE_S3_DIR))
        files.append(os.path.relpath(file, FAKE_S3_DIR))
    return files


def _mock_download_file(bucket, remoteFile, localFile):
    _mock_get_archive('s3://tdar/%s/%s' % (bucket, remoteFile), localFile)


class BaseTest(unittest.TestCase):
    # _put_archive = tasks.put_archive
    # _get_archive = tasks.get_archive
    # _get_backup_files = tasks.get_backup_files
    # _download_file = tasks._download_file

    def sandbox(self, *args):
        """ return path to a test-specific directory (for temp files and such)
        :param args: optional list of paths to append to sandbox path.
        :return: abs path to test sandbox, or abs path to file/dir specified by arglist
        """
        # subdir = self.id()
        testdir = self._testMethodName
        return resolve(SANDBOX_ROOT_DIR, testdir, *args)

    @classmethod
    def setUpClass(cls):

        shutil.rmtree(TARGET_DIR, ignore_errors=True)
        os.makedirs(TARGET_DIR)
        shutil.copytree(RESOURCE_DIR + '/filestore', SANDBOX_ROOT_DIR)
        os.makedirs(FAKE_S3_DIR)

        # don't actually send/receive stuff in tests
        cls._put_archive = tasks.put_archive
        cls._get_archive = tasks.get_archive
        cls._get_backup_files = tasks.get_backup_files
        cls._download_file = tasks.download_file

        # tasks.put_archive = _fake_method('put_archive')
        tasks.put_archive = _mock_put_archive

        tasks.get_archive = _fake_method('get_archive')
        tasks.get_backup_files = _mock_get_backup_files
        tasks._download_file = _fake_method('_download_file')

    @classmethod
    def tearDownClass(cls):
        tasks.put_archive = cls._put_archive
        tasks.get_archive = cls._get_archive
        tasks.get_backup_files = cls._get_backup_files

    def setUp(self):
        # recreate target dir and copy sandbox files into it
        if os.path.exists(test_config_file_path):
            os.remove(test_config_file_path)

        # build a customized test config.ini file
        with open(test_config_file_path, 'w') as test_config_file:
            cfg = configparser.ConfigParser()
            cfg.read(resolve(PWD, '../config.template.ini'))
            cfg.defaults()['magic_word'] = 'this is a test!'
            cfg.defaults()['manifests_dir'] = self.sandbox('manifests')
            cfg.write(test_config_file)

        os.makedirs(self.sandbox())

        # the executor normally handles prerequisite tasks, but we have to manually do it in a test(for now)
        tasks.prepare()
        tasks.validate()

    def tearDown(self):
        pass

    # create file at specified path containing random bytes
    def gibberish_file(self, path, bytecount=1024):
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        with open(path, 'wb') as fout:
            fout.write(os.urandom(bytecount))

    # return list of all files contained in root directory and it's sub-directories
    def list_files(self, rootdir):
        return [resolve(root, file) for root, dirs, files in os.walk(rootdir) for file in files]

    def copy_filestore_to_sandbox(self, name='filestore'):
        """ duplicate the sample filestore to current test's sandbox
        :param name:
        :return: path to local copy of filestore
        """
        fsdir = self.sandbox(name)
        shutil.copytree(resolve(RESOURCE_DIR, 'filestore'), fsdir)
        return fsdir


class SyncUtilsTest(BaseTest):
    # lots of stuff to check when verifying generate_diffs. so we bust out setup into a helper method
    def _setup_generate_diffs(self):
        """ execute generate_diffs in your test sandbox and returns list of expected  added/modified/deleted files
        :return: files_added, files_modified, files_removed
        """
        manifest1 = self.sandbox('manifest1.txt')
        manifest2 = self.sandbox('manifest2.txt')
        # copy the filestore to sandbox since we intend to modify files
        fsdir = self.sandbox('filestore')
        diffdir = self.sandbox('diffs')
        os.makedirs(diffdir)
        shutil.copytree(resolve(RESOURCE_DIR, 'filestore'), fsdir)
        allfiles = self.list_files(fsdir)

        # take initial manifest, then modify the filesystem (delete, modify, add)
        tasks.build_manifest(fsdir, manifest1)

        self.assertGreater(len(allfiles), 4, "sample filestore must have at least 4 files for this test "
                                             "to work properly")
        # remove last two files in file store
        # modify first two files in file store
        # add two gibberish files in two different directories
        files_removed = allfiles[-2:]
        files_modified = allfiles[:2]
        files_added = [resolve(fsdir, 'collection/newfile1.bin'), resolve(fsdir, 'collection/10/10/0/newfile2.bin')]

        for file in files_added:
            self.gibberish_file(file)
            logger.debug("adding file:%s", file)
        for file in files_modified:
            with open(file, "a") as outfile:
                logger.debug("modifying file:%s", file)
                outfile.write("added text")
        for file in files_removed:
            logger.debug("deleting file:%s", file)
            os.remove(file)

        tasks.build_manifest(fsdir, manifest2)
        tasks.generate_diffs(manifest1, manifest2, diffdir)

        return files_added, files_modified, files_removed

    def test_gibberish_file(self):
        self.gibberish_file(self.sandbox('fubu.bin'))
        self.assertTrue(self.sandbox('fubu.bin'))

    def test_manifest_to_listing(self):
        listing_file_path = self.sandbox('list.txt')
        logger.debug('listing_file_path:%s', listing_file_path)
        self.assertFalse(os.path.exists(listing_file_path))
        self.assertTrue('test/resources/manifest.txt')
        tasks.manifest_to_listing('test/resources/manifest.txt', listing_file_path)
        # should also verify that listing file contains same contents as manifest
        self.assertTrue(os.path.exists(listing_file_path))

    def test_prepare(self):
        # setup probably created this already, so delete it.
        if os.path.exists(test_config_file_path):
            os.remove(test_config_file_path)
        self.assertFalse(os.path.exists(test_config_file_path))
        tasks.prepare()
        self.assertTrue(os.path.exists(test_config_file_path))

    def test_encrypt(self):
        infile = resolve(SANDBOX_ROOT_DIR, 'collection/10/10/0/rec/record.2014-03-30--23-55-23.xml')
        outfile = self.sandbox('file.encrypted')
        self.assertTrue(os.path.exists(infile))

        tasks.encrypt(infile, outfile)
        self.assertTrue(os.path.exists(outfile), "expected to find %s" % outfile)

    def test_decrypt(self):
        infile = resolve(SANDBOX_ROOT_DIR, 'collection/10/10/0/rec/record.2014-03-30--23-55-23.xml')
        outfile = self.sandbox('file.encrypted')
        decfile = self.sandbox('file.decrypted')
        tasks.encrypt(infile, outfile)
        tasks.decrypt(outfile, decfile)
        self.assertTrue(filecmp.cmp(infile, decfile), "decypted file should be same as original")

    def test_tarcrypt(self):
        manifestFile  = self.sandbox('manifest.txt')
        listingFile = self.sandbox('listing.txt')
        srcdir = resolve(SANDBOX_ROOT_DIR, 'collection')
        tarFile = self.sandbox('out.tar.gpg')
        tasks.build_manifest(archive_directory=srcdir, outfile=manifestFile)
        tasks.manifest_to_listing(manifestFile, listingFile)
        tasks.tarcrypt(listingFile, tarFile)

        #okay, now decrypt the contents and make sure they have same files
        os.makedirs(self.sandbox('decfiles'))
        tasks.decrypt(tarFile, self.sandbox('out.tar'))
        tasks.untar(self.sandbox('out.tar'), self.sandbox('decfiles'))
        allfiles = self.list_files(srcdir)
        with open(listingFile, 'r') as file:
            for line in file:
                path = line.rstrip()
                self.assertTrue(any(path in file for file in allfiles), 'file must exist in tar: {}'.format(path))


    def test_manifest(self):
        outfile = self.sandbox('manifest.txt')
        srcdir = resolve(SANDBOX_ROOT_DIR, 'collection')

        # get our own listing, compare it to manifest later
        allfiles = self.list_files(srcdir)
        tasks.build_manifest(archive_directory=srcdir, outfile=outfile)
        with open(outfile, 'r') as file:
            # remove newlines, then convert to list of entries with each entry having [hashcode, path]
            manifest_entries = [line.rstrip().split(maxsplit=1) for line in file]

            for line in manifest_entries:
                logger.debug('type:%s  line:%s', type(line), line)

            # check for correct filecount
            manifest_filecount = len(manifest_entries)
            expected_filecount = len(allfiles)
            self.assertGreater(manifest_filecount, 0, 'source folder should have at least one file')
            self.assertEqual(expected_filecount, manifest_filecount,
                             "manifest linecount should match # of files in source folder")

            # make sure all files accounted for
            for chksum, path in manifest_entries:
                self.assertTrue(os.path.exists(path), 'expected path to exist:' + path)

    def test_generate_diffs1(self):
        files_added, files_modified, files_removed = self._setup_generate_diffs()
        diffdir = self.sandbox('diffs')

        # assert that files_deleted has same entries as diffdir/deleted.txt
        deletefile = resolve(diffdir, 'deleted.txt')
        self.assertTrue(os.path.exists(deletefile), 'expected deleted.txt')
        with open(deletefile, 'r') as file:
            linecount = sum(1 for line in file)
            self.assertEqual(linecount, len(files_removed), "deleted.txt should have same itemcount as files_removed")
            file.seek(0)
            for line in file:
                path = line.rstrip()
                logger.debug('looking for %s', line)
                self.assertTrue(any(path in file_removed for file_removed in files_removed),
                                'deleted.txt should contain %s' % path)

    def test_generate_diffs2(self):
        files_added, files_modified, files_removed = self._setup_generate_diffs()
        diffdir = self.sandbox('diffs')

        # assert that diffdir/added.txt contains files_added & files_modified (and nothing else!)
        newfile = resolve(diffdir, 'new.txt')
        self.assertTrue(os.path.exists(newfile), 'expected to find new.txt')
        with open(newfile, 'r') as file:
            linecount = sum(1 for line in file)
            self.assertEqual(linecount, len(files_added) + len(files_modified),
                             "new.txt should have same itemcount as files_removed")
            file.seek(0)
            for line in file:
                path = line.rstrip()
                logger.debug('looking for %s', line)
                self.assertTrue(
                        any(path in f for f in files_added) or any(path in f for f in files_modified)
                        , 'new.txt should contain %s' % path)

    def test_manifest_dir_with_symlinks(self):
        """ assert that manifest does not follow symlinks
        """
        os.makedirs(self.sandbox('filestore'))
        gibfile = self.sandbox('filestore/gibberish.bin')
        symlink_file = self.sandbox('filestore/gibberish.symlink.bin')

        # create gibberish file and make a symlink to it
        self.gibberish_file(gibfile)
        os.symlink(gibfile, symlink_file)

        # assert that symlink file not included in listing
        file_list1 = self.list_files(self.sandbox('filestore'))
        self.assertEqual(len(file_list1), 2)
        tasks.build_manifest(archive_directory=self.sandbox('filestore'), outfile=self.sandbox('manifest.txt'))
        linecount = count_lines(self.sandbox('manifest.txt'))
        self.assertEqual(linecount, 1)

    def test_task_should_exit_after_bad_shell_command(self):
        with self.assertRaises(SystemExit):
            tasks.encrypt('/this/file/does/not/exist', '/neither/does/this')

    def test_prepare_manifest_dir(self):
        tasks.prepare_manifest_dir('my_fancy_backup')
        self.assertTrue(os.path.exists(self.sandbox('manifests/my_fancy_backup')))

    # note: transfer to/from s3 is mocked.
    def test_backup(self):
        fsdir = self.sandbox('filestore')
        source_dir = self.sandbox('filestore')
        staging_dir = self.sandbox('staging')

        shutil.copytree(resolve(RESOURCE_DIR, 'filestore'), fsdir)
        os.makedirs(staging_dir)
        self.assertTrue(os.path.exists(source_dir))
        backup_name = 'backuptest1'
        tasks.backup(source_dir, staging_dir, backup_name)

    def test_list_backupFiles(self):
        """ perform two backups (backup1, backup2), but request listing from only backup1
        """
        source_dir = self.copy_filestore_to_sandbox()
        staging_dir = self.sandbox('staging')

        os.makedirs(staging_dir)
        self.assertTrue(os.path.exists(source_dir))
        tasks.backup(source_dir, staging_dir, 'backup1')
        tasks.backup(source_dir, staging_dir, 'backup2')

        listing1 = sorted(tasks.list_backup_files('backup1'))
        self.assertEqual(len(listing1), 2)
        self.assertTrue(listing1[0].endswith('/backup1/snap/backup.tar.gpg'))
        self.assertTrue(listing1[1].endswith('/backup1/snap/manifest.txt'))

    def test_incremental_manifests(self):
        source_dir = self.copy_filestore_to_sandbox()
        staging_dir = makedirs(self.sandbox('staging'))
        backup_name = 'inctest'
        self.assertTrue(os.path.exists(source_dir))
        tasks.backup(source_dir, staging_dir, backup_name)

        # add a gibberish file and then do an incremental backup
        self.gibberish_file(self.sandbox('filestore/abc123.bin'))
        tasks.incremental_backup(source_dir, staging_dir, backup_name)

        # add more files and take another incremental backup
        self.gibberish_file(self.sandbox('filestore/def345.bin'))
        self.gibberish_file(self.sandbox('filestore/delete-me.bin'))
        tasks.incremental_backup(source_dir, staging_dir, backup_name)

        # now remove a file and make another incremental backup
        os.remove(self.sandbox('filestore/delete-me.bin'))
        tasks.incremental_backup(source_dir, staging_dir, backup_name)

        # check out the manifests and make sure they have the right things
        backup1_added = file_to_list(self.sandbox('manifests/inctest/deltas/delta1/new.txt'))
        backup2_added = file_to_list(self.sandbox('manifests/inctest/deltas/delta2/new.txt'))
        backup3_added = file_to_list(self.sandbox('manifests/inctest/deltas/delta3/new.txt'))
        backup3_deleted = file_to_list(self.sandbox('manifests/inctest/deltas/delta3/deleted.txt'))
        self.assertIn(self.sandbox('filestore/abc123.bin'), backup1_added, 'expected abc123 in added.txt')
        self.assertIn(self.sandbox('filestore/def345.bin'), backup2_added, 'expected def345 and delete-me in added.txt')
        self.assertIn(self.sandbox('filestore/delete-me.bin'), backup2_added,
                      'expected def345 and delete-me in added.txt')
        self.assertIn(self.sandbox('filestore/delete-me.bin'), backup3_deleted, 'expected delete-me.bin in deleted.txt')
        self.assertListEqual(backup3_added, [], 'expected added.txt to be blank (no files added)')

    def x_test_list_backupFiles(self):
        self.fail('not implemented')


if __name__ == '__main__':
    unittest.main()
