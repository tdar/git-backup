# TDAR Backup Utility
A tool for creating, sending, and retreiving filesystem backups on the Amazon Glacier storage service.

## Usage
    invoke [command][options]

Commands can receive options and option values in three ways; single-dash flags, double-dash flags (named arguments),
or as positional arguments).  The following three commands are equivalent.
````
invoke encrypt -i myfile.txt -o result.bin
invoke encrypt --input-file=myfile.txt --output-file=result.bin
invoke encrypt myfile.txt result.bin
````
## Commands

Every command has at least one required option.  To learn about a particular command and what options are available,
run the following command
````
invoke -h [command_name]
````
| Command | Description |
| ------- | ----------- |
| backup | Compress, encrypt directory then send to s3 |
| incremental_backup  | Perform an incremental backup, relative to a previous full backup w/ the same backup name. |
| restore_backup      | Restore a backup - including snapshot and all incremental backups. |
| build_manifest      | Generate a manifest file for the specified directory. |
| decrypt             | Decrypt a file or directory. |
| encrypt             | Encrypt a file or directory. |
| generate_diffs      | Compare two manifest files and generate two 'diff' listings. |
| get_archive         | Retrieve a file from S3 |
| list_backup_files   | List the files contained in the specified backup name. |
| put_archive         | Send file to S3. |

### backup

Compress, encrypt directory then send to s3

#### Parameters
    dry_run : if true, create archive and manifest but do not upload them to glacier
    source_dir : directory to process
    staging_dir : where to place temporary/working files
    backup_name : name of the backup.  This will serve as the prefix for the archive file name in S3

#### Options
    -b STRING, --backup-name=STRING
    -d, --dry-run
    -s STRING, --source-dir=STRING
    -t STRING, --staging-dir=STRING

### incremental_backup
  Perform an incremental backup, relative to a previous full backup w/ the same backup name.

#### Parameters
    source_dir: source directory
    staging_dir: where to place temporary, working files
    backup_name: backup name (must match a previous full backup name)

#### Options
    -b STRING, --backup-name=STRING
    -s STRING, --source-dir=STRING
    -t STRING, --staging-dir=STRING
    
    
### restore_backup

#### Parameters
    backup_name : name of the backup to restore (ostensibly the name chosen when using the 'backup' or 'incremental_backup' command)
    target_dir : path that will receive the restored backup files 
    staging_dir : path to use for temporary files
    dry_run : if true,  validate the backup name and target directory but do not attempt to download / restore a backup

#### Options
    -b STRING, --backup-name=STRING
    -d, --dry-run
    -s STRING, --staging-dir=STRING
    -t STRING, --target-dir=STRING





## See Also
* For information regarding installation and configuration of this utility, consult `installation-notes.md` which should be located in the same directory that contains this readme.

* This utility uses the Python invoke library to assist in parsing commands and command-line arguments (https://github.com/pyinvoke/invoke).