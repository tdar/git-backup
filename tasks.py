from invoke import run, task
from os import path

import configparser
import os
import shutil
import sys
import boto3
import boto3.session
import logging
import invoke.exceptions
import glob
import time
from scandir import scandir
import hashlib
'''
    todo:
        - testing:  gracefully handle trailing slash on source)_dir (e.g. '/data/filestore/' vs. '/data/filestore')
'''

LISTING_FILE_ADDED = 'new.txt'
LISTING_FILE_DELETED = 'deleted.txt'
MANIFEST_FILE = 'manifest.txt'
MANIFEST_FILE_NEW = 'manifest-new.txt'
MANIFEST_FILE_OLD = 'manifest-old.txt'
BACKUP_TAR = 'backup.tar'
BACKUP_GPG = 'backup.tar.gpg'
DELTAS_DIR = 'deltas'
SNAP_DIR = 'snap'
DEFAULT_MAGIC_WORD = 'MAGIC_WORD_GOES_HERE'

# retry a failed send 15 times, waiting 15 minutes each time  (so, timeout in about 5 hours)
PUT_MAX_RETRIES = 15
PUT_RETRY_WAIT = 15 * 60


# fixme: this is unreliable, as code & error text may change w/ future versions
PARTIAL_ERROR_TEXT = 'failed too many times'
EX_PARTIAL = 2


CONFIG_DEFAULTS = {
    'manifest_cmd': 'shasumr-sort.sh',
    'appname': 'backup',
    'log_level': 'INFO'
}

DEFAULT_CONFIG_FILE_PATH = path.expanduser('~/.tdarbackup/config.ini')

logging.basicConfig(stream=sys.stdout, )
logger = logging.getLogger(__name__)

# todo figure out how to set this to 'verbose' (hopefully without adding verbose arg to each task definition)
logger.setLevel('DEBUG')


def load_config():
    config = configparser.ConfigParser(CONFIG_DEFAULTS)
    config.read(DEFAULT_CONFIG_FILE_PATH)
    return config


class BackupConfig(object):

    def __init__(self, config_file_path=None, config_default_dict=None, *args, **kwargs):
        if config_file_path is None:
            config_file_path = DEFAULT_CONFIG_FILE_PATH
        if config_default_dict is None:
            config_default_dict = CONFIG_DEFAULTS
        self._config = configparser.ConfigParser(config_default_dict)
        self._config.read(config_file_path)

    @property
    def baseurl(self):
        return "s3://{0}".format(self._config['AWS']['default_bucket'])

    def backup_url(self, backup_name):
        return "{0}/{1}/".format(self.baseurl, backup_name)


def run_maybe(cmd, dry_run=False):
    if dry_run:
        logger.info('dry-run enabled - skipping this command: %s', cmd)
    else:
        run_safe(cmd)


def run_safe(cmd):
    """execute a shell command. Perform sys.exit if command fails

    Parameters
    ----------
    cmd: command to execute
    """
    result = None
    try:
        result = run(cmd, echo=True, hide=False)
    except invoke.exceptions.Failure as err:
        logger.error("command returned code %s - terminating", err.result.return_code)
        fail('exiting')
    return result


def hash_cmd(f):
    print('hash command for: ' + f)


def recursive_hash(src, exclude_pattern=None):
    for root, dirs, files in os.walk(src, followlinks=False):
        if exclude_pattern is not None:
            files = [f for f in files if f not in glob.glob(path.join(root, exclude_pattern))]
        for filename in files:
            hash_cmd(resolve(root, filename))

@task
def prepare():
    if not path.isfile(DEFAULT_CONFIG_FILE_PATH):
        logger.info("Config file not present - creating new config at %s" % DEFAULT_CONFIG_FILE_PATH)
        if not path.exists(path.dirname(DEFAULT_CONFIG_FILE_PATH)):
            os.makedirs(path.dirname(DEFAULT_CONFIG_FILE_PATH))
        shutil.copyfile('config.template.ini', DEFAULT_CONFIG_FILE_PATH)
        # (re)set logging level
        logger.setLevel(load_config().defaults()['log_level'])


@task(prepare)
def validate():
    cfg = load_config()
    if 'magic_word' not in cfg.defaults() or cfg.defaults()['magic_word'] == DEFAULT_MAGIC_WORD:
        fail('No magic word specified. Please add a magic word to the configuration file')


@task(validate)
def encrypt(infile, outfile, keep_original=True):
    """ Encrypt a file or directory.

    Parameters
    ----------
    infile: file or directory to encrypt
    outfile: destination for encrypted file
    keep_original: true to keep original (the default), false to delete
    """
    config = load_config()
    run_safe("gpg2 --yes --batch --passphrase='{}' --cipher-algo AES256 -c -o '{}' '{}' ".format(
        config.defaults()['magic_word'], outfile, infile))
    if not keep_original:
        os.remove(infile)


@task(validate)
def decrypt(infile, outfile, keep_original=True):
    """ Decrypt a file or directory.

    Parameters
    ----------
    infile: encrypted file
    outfile: destination for decrypted file(s)
    keep_original: true to keep encrypted file after decryption (the default),  false to delete
    """
    magic_word = load_config().defaults()['magic_word']
    command = "gpg2 --yes --batch --passphrase='{}' --output='{}'  '{}'".format(magic_word, outfile, infile)
    logger.debug(command)
    run(command)
    if not keep_original:
        os.remove(infile)


# scan through the directory tree recursively, scandir doesn't stat every file so, it's fast, and the filename contains is much
# faster than a regex
def dir(rootfolder):
        for entry in scandir(rootfolder):
            if ( "/deriv/" not in entry.path and "/support/" not in entry.path and ".foaf." not in entry.path ):
              if (entry.is_dir()):
                   yield from dir(entry.path)  # see below for Python 2.x
              else:
                   yield entry


## create a sha1 digest with each 4kb of the file
def digest(fname):
    hash_md5 = hashlib.sha1()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)

    return hash_md5.hexdigest()



@task(validate)
def build_manifest(archive_directory, manifest_file='manifest.txt'):
    """ Generate a manifest file for the specified directory.

    Parameters
    ----------
    archive_directory: the source path (must be directory)
    manifest_file: path for destination manifest file
    """
    config = load_config()
    manifest_cmd = config.defaults().get('manifest_cmd')

    logger.debug("running manifest_cmd:{} on dir:{} and generating output manifest:{}".format(
        manifest_cmd, archive_directory, manifest_file))

#    manifest_file = '/tmp/sha.txt'

    # write the digest to a file
    with open(manifest_file,'w') as outfile:
        for entry in dir(archive_directory ):
            outfile.write("{}  {}\n" . format( digest(entry.path) , entry.path ))

#    run_safe("sort {} > {}".format(manifest_cmd, manifest_file, outfile))


# strip hashes from a manifest file so that it can be used with tar or rsync
# note: This function could (should?) be replaced with `cat /path/to/manifest.txt | cut -d' ' -f3`
def manifest_to_listing(manifest_file='manifest.txt', outfile='/tmp/listing.txt'):
    """ Output a manifest file minus the hashcode values.

    Parameters
    ----------
    manifest_file : path to manifest file
    outfile : path to output file
    """
    with open(manifest_file, 'r') as infile, open(outfile, 'w') as outfile:
        for crc, p in [line.rstrip().split(maxsplit=1) for line in infile]:
            outfile.write(p + '\n')
            logger.debug(p)


def sort_file(file, outfile=None):
    if outfile is None:
        outfile = file
    run_safe("sort {} -o {}".format(file, outfile))


@task
def generate_diffs(manifest1, manifest2, destination_folder='.', already_sorted=False):
    """Compare two manifest files and generate two 'diff' listings.

    Compare two manifest files and generate two 'diff' listings: 'deleted' and 'modified'
        - the 'deleted' listing contains files in manifest1 that are present in manifest2.
        - the 'new' listing contains files that are exclusive to manifest1 (i.e. new files) and
          files that appear in both manifests but have different hash codes

    Taken together,  these files reflect the files that a differential backup contains, and
    also specifies which files to delete after applying a restore from a differential backup
    in order to create a file structure that exactly matches manifest2.


    Parameters
    ----------
    manifest1 : file path to the previous backup manifest
    manifest2 : file path the most recent backup manifest
    destination_folder : where to store the diff listing files
    already_sorted : true if the manifest files are already sorted
    """

    manifest1_sorted = destination_folder + "/" + "manifest1.sorted.txt"
    manifest2_sorted = destination_folder + "/" + "manifest2.sorted.txt"

    if already_sorted:
        shutil.copyfile(manifest1, manifest1_sorted)
        shutil.copyfile(manifest2, manifest2_sorted)
    else:
        logger.debug('sorting manifest1')
        sort_file(manifest1, manifest1_sorted)
        logger.debug('sorting manifest2')
        sort_file(manifest2, manifest2_sorted)

    # build the 'new' listing (modified or new files in manifest2)
    logger.debug("generating newfile")
    run("comm -13 {} {} > {}/new.manifest.txt".format(manifest1_sorted, manifest2_sorted, destination_folder))
    manifest_to_listing(destination_folder + '/new.manifest.txt', destination_folder + '/' + 'new.txt')
    sort_file(destination_folder + '/new.txt')

    # build the 'deleted' listing
    logger.debug("generating deletefile")
    manifest_to_listing(manifest1, destination_folder + '/listing1.txt')
    manifest_to_listing(manifest2, destination_folder + '/listing2.txt')
    sort_file(destination_folder + '/listing1.txt')
    sort_file(destination_folder + '/listing2.txt')
    run_safe("comm -23 {0}/{1} {0}/{2} > {0}/deleted.txt".format(destination_folder, 'listing1.txt', 'listing2.txt'))

    # remove working files
    os.remove(resolve(destination_folder, 'listing1.txt'))
    os.remove(resolve(destination_folder, 'listing2.txt'))
    os.remove(manifest1_sorted)
    os.remove(manifest2_sorted)


def tar(listing_file, outfile):
    return run_safe("tar cf {} -T {}".format(outfile, listing_file))


def tarcrypt(listing_file, outfile):
    """
    Generate an encrypted tar containing files specified by a listing file.

    Parameters
    ----------
    listing_file : path to file containing file paths to include in resultant tar.gpg file.
    outfile : destination path of the tar.gpg file
    """
    config = load_config()
    run_safe(" tar c -T {} | gpg2 --yes --batch --passphrase='{}' --cipher-algo AES256 -c -o '{}' "
             .format(listing_file, config.defaults()['magic_word'], outfile))


@task(validate)
def put_archive(local_file, s3url, dry_run=False):
    """ Send file to S3.
    local_file : path to file to send
    s3url : full URL to destination
    dry_run : if true, confirm source file exists but don't perform transfer (default true
    """
    _cmd = "s3cmd --no-guess-mime-type --continue-put --multipart-chunk-size-mb=150 put {} {}".format(local_file, s3url)
    logger.debug('sending file:' + _cmd)
    if dry_run:
        return

    # try to send file. if we get back EX_PARTIAL, wait awhile and try until retries exhausted
    code = EX_PARTIAL
    retries = PUT_MAX_RETRIES
    while code == EX_PARTIAL and retries > 0:
        result = run(_cmd, echo=True, hide=False)
        code = result.return_code
        if code == 0:
            return code
        else:
            time.sleep(PUT_RETRY_WAIT)
            retries -= 1
    if code != 0:
        fail("failed to send file, s3cmd returned code {}".format(code))

@task(validate)
def get_archive(s3url, local_file, dry_run=False):
    """ Retrieve a file from S3

    Parameters
    ----------
    s3url : full URL to archive
    local_file : path to output file
    dry_run : if true, confirm file exists but don't perform transfer (default false)

    """
    run_maybe("s3cmd get -r --continue {} {}".format(s3url, local_file), dry_run)


def aws_session():
    aws = load_config()['AWS']
    session = boto3.session.Session(
        aws_access_key_id=aws['aws_access_key_id'],
        aws_secret_access_key=aws['aws_secret_access_key'],
        region_name=aws['region_name'])
    return session, aws


def get_backup_files(backup_name):
    session, awsconfig = aws_session()
    s3 = session.resource('s3')
    bucket = s3.Bucket(awsconfig['default_bucket'])
    files = []
    for obj in bucket.objects.filter(Prefix=backup_name):
        files.append(obj.key)
    return files


@task(validate)
def list_backup_files(backup_name):
    """ List the files contained in the specified backup name.
    backup_name : name of the backup (e.g. 'tdar-2015-q1', not 's3://tdar/backup-2015-q1/backup.tgz'
    """
    backupfiles = get_backup_files(backup_name)
    for filename in backupfiles:
        logger.info(filename)
    return backupfiles


def makedirs(dir_name):
    """ similar to os.makedirs but does not throw exception if directory already present.

    Parameters
    ----------
    dir_name : directory path to create if not present
    """
    dir_name = resolve(dir_name)
    if not path.exists(dir_name):
        os.makedirs(dir_name)
    return dir_name


def prepare_manifest_dir(backup_name):
    """ Create the top-level manifests direcory for the specified backup name, if it doesn't already exist.

    Parameters
    ----------
    backup_name : name of the backup

    Returns
    -------
    Path to manifests directory
    """
    manifests_dir = resolve(load_config().defaults()['manifests_dir'], backup_name)
    makedirs(resolve(manifests_dir, 'snap'))
    makedirs(resolve(manifests_dir, 'deltas'))
    return manifests_dir


def get_manifest_files(backup_name):
    manifests_dir = prepare_manifest_dir(backup_name)
    files = [resolve(root, file) for root, dirs, files in os.walk(manifests_dir) for file in files]
    return files


@task(validate)
def backup(source_dir, staging_dir, backup_name, dry_run=False):
    """ Compress, encrypt directory then send to s3

    Parameters
    ----------
    dry_run : if true, create archive and manifest but do not upload them to glacier
    source_dir : directory to process
    staging_dir : where to place temporary/working files
    backup_name : name of the backup.  This will serve as the prefix for the archive file name in S3
    """
    working_dir = makedirs(resolve(staging_dir, backup_name))
    manifests_dir = prepare_manifest_dir(backup_name)
    manifest_file = resolve(working_dir, 'manifest.txt')
    backup_dir = path.dirname(manifest_file)
    tar_file = resolve(working_dir, backup_name + '.tar')
    gpg_file = tar_file + '.gpg'
    c = BackupConfig()
    baseurl = c.backup_url(backup_name)
    listing_file = resolve(working_dir, 'listing.txt')
    makedirs(backup_dir)
    build_manifest(source_dir, manifest_file)
    manifest_to_listing(manifest_file, listing_file)
    tarcrypt(listing_file, gpg_file)
    put_archive(manifest_file, baseurl + 'snap/manifest.txt', dry_run)
    put_archive(gpg_file, baseurl + 'snap/backup.tar.gpg', dry_run)
    shutil.copyfile(manifest_file, resolve(manifests_dir, 'snap/manifest.txt'))


# return dictionary containing: snap filename, list of delta file names
def delta_list(backupfiles):
    deltas = []
    for filename in backupfiles:
        if '/deltas/' in filename:
            _dir = filename[0:filename.rfind('/')]
            if _dir not in deltas:
                deltas.append(_dir)
    return deltas


# same as os.path.join, but also expands variables and normalizes the result
def resolve(*args):
    # expand, join, then normalize
    root_path = path.expanduser(path.expandvars(args[0]))
    final_path = path.abspath(
        path.join(
            root_path,
            *[path.expanduser(path.expandvars(p)) for p in args[1:]]
        )
    )
    return final_path

"""
    _paths = [os.path.expanduser(path) for path in args]
    _paths = [os.path.expandvars(path) for path in _paths]
    _path = os.path.join(*_paths)
    _path = os.path.normpath(_path)
    # remove leading double-slash
    if _path[:2] == '//':
        _path = _path[1:]
    return _path
"""


def s3_client():
    session, aws = aws_session()
    client = session.client('s3')
    return client


def download_file(bucket, remotefile, localfile):
    s3_client().download_file(bucket, remotefile, localfile)


@task(validate)
def incremental_backup(source_dir, staging_dir, backup_name):
    """ Perform an incremental backup, relative to a previous full backup w/ the same backup name.
    Parameters
    ----------
    source_dir: source directory
    staging_dir: where to place temporary, working files
    backup_name: backup name (must match a previous full backup name)
    """
    backupfiles = get_manifest_files(backup_name)
    deltas = delta_list(backupfiles)
    bucket = load_config()['AWS']['default_bucket']
    source_dir = resolve(source_dir)
    staging_dir = resolve(staging_dir)
    manifests_dir = prepare_manifest_dir(backup_name)
    manifest_file_old = resolve(staging_dir, backup_name, 'manifest-old.txt')
    manifest_file_new = resolve(staging_dir, backup_name, 'manifest-new.txt')
    listing_file = resolve(staging_dir, backup_name, LISTING_FILE_ADDED)
    deleted_file = resolve(staging_dir, backup_name, LISTING_FILE_DELETED)
    tar_file = resolve(staging_dir, backup_name, BACKUP_TAR)
    gpg_file = resolve(staging_dir, backup_name, BACKUP_GPG)
    baseurl = 's3://%s/%s/' % (bucket, backup_name)

    # pull down the most recent manifest
    makedirs(resolve(staging_dir, backup_name))
    if len(deltas) > 0:
        old_manifest = resolve(manifests_dir, DELTAS_DIR, 'delta' + str(len(deltas)), 'manifest.txt')
    else:
        old_manifest = resolve(manifests_dir, 'snap/manifest.txt')

    logger.info('Downloading previous manifest at %s to %s' % (old_manifest, manifest_file_old))
    shutil.copyfile(old_manifest, manifest_file_old)

    # make a new manifest
    build_manifest(source_dir, manifest_file_new)
    generate_diffs(manifest_file_old, manifest_file_new, resolve(staging_dir, backup_name))

    tar(listing_file, tar_file)
    encrypt(tar_file, gpg_file, keep_original=False)

    # send files to s3
    delta_dir = 'deltas/delta' + (str(len(deltas) + 1))
    baseurl = baseurl + delta_dir + '/'
    put_archive(manifest_file_new, baseurl + MANIFEST_FILE)
    put_archive(gpg_file, baseurl + BACKUP_GPG)
    put_archive(listing_file, baseurl + LISTING_FILE_ADDED)
    put_archive(deleted_file, baseurl + LISTING_FILE_DELETED)

    # copy manifests to local manifest dir
    makedirs(resolve(manifests_dir, delta_dir))
    shutil.copyfile(manifest_file_new, resolve(manifests_dir, delta_dir, MANIFEST_FILE))
    shutil.copyfile(listing_file, resolve(manifests_dir, delta_dir, LISTING_FILE_ADDED))
    shutil.copyfile(deleted_file, resolve(manifests_dir, delta_dir, LISTING_FILE_DELETED))


def fail(reason='An error occurred'):
    sys.exit(reason)


def untar(tarfile, target_dir):
    command = "tar -x -f '{}' -C '{}' ".format(tarfile, target_dir)
    logger.debug(command)
    return run(command)


def process_incremental(incremental_dir, target_dir):
    logger.info('processing incremental backup at {}'.format(incremental_dir))
    # decrypt/extract the incremental
    gpg_file = resolve(incremental_dir, BACKUP_GPG)
    tar_file = resolve(incremental_dir, BACKUP_TAR)
    decrypt(gpg_file, tar_file)
    untar(tar_file, target_dir)

    # process deletions
    with open(resolve(incremental_dir, LISTING_FILE_DELETED), 'r') as deleted_files:
        for filepath in deleted_files:
            filepath = filepath.strip()
            # convert absolute path to relative
            if filepath[0] == '/':
                filepath = filepath[1:]
            file_to_delete = resolve(target_dir, filepath)
            logger.info('deleting orphan file: {}'.format(file_to_delete))
            os.remove(file_to_delete)


@task(validate)
def restore_backup(backup_name, target_dir, staging_dir, dry_run=False):
    """ Restore a backup - including snapshot and all incremental backups.

    Parameters
    ----------
    backup_name : name of the backup to restore (ostensibly the name chosen when using the 'backup' or 'incremental_backup' command)
    target_dir : path that will receive the restored backup files
    staging_dir : path to use for temporary files
    dry_run : if true,  validate the backup name and target directory but do not attempt to download / restore a backup

    """
    bucket = load_config()['AWS']['default_bucket']
    backup_files = get_backup_files(backup_name)
    s3url = 's3://{}/{}'.format(bucket, backup_name)

    # get_archive(s3url, local_file, dry_run=False)
    # validate
    if not os.path.exists(target_dir):
        fail('Target not found:' + target_dir)
    if not os.path.isdir(target_dir):
        fail('Target is a file:' + target_dir)
    if len(backup_files) == 0:
        fail('No files found at {}'.format(s3url))

    # pull down files to staging
    staging_dir = makedirs(staging_dir)
    logger.info('attempting to retrieve files from: %s', s3url)
    get_archive(s3url, staging_dir, dry_run)

    # decrypt/extract the snapshot
    gpg_file = resolve(staging_dir, backup_name, 'snap/backup.tar.gpg')
    tar_file = resolve(staging_dir, backup_name, 'snap/backup.tar')
    decrypt(gpg_file, tar_file)
    untar(tar_file, target_dir)

    # decrypt/extract any incremental backups
    deltas_dir = resolve(staging_dir, backup_name, DELTAS_DIR)
    deltas = next(os.walk(deltas_dir))[1]
    # TODO: don't assume this list will be sorted. start with delta1, then delta2, and so on
    for i in range(len(deltas)):
        process_incremental(resolve(deltas_dir, 'delta' + str(i+1)), target_dir)

