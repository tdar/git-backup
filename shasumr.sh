#!/bin/bash

#don't hog cpu or io
#ionice -c3 -p$$
#renice +15 -p $$

# where to place the results 
targetdir=$PWD
hash_cmd=shasum

if [ $# -gt 0 ]; then 
    targetdir=$1
fi

#find "$targetdir" -type f  -not -path "*/deriv/*" -not -path "*/creator/*/support/*" -not -name "*.foaf.*" -exec shasum {} \; 
find "$targetdir" -type f  -not -path "*/deriv/*" -not -path "*/creator/*/support/*" -not -name "*.foaf.*" -exec $hash_cmd {} \;
