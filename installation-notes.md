# Installation Notes

## Summary
Befure using the backup utility it must be properly installed and configured,  and certain prerequisites must be met.  

## Installation

### Install tdar-backup scripts   
> Make sure you log in as the the same user that will execute these scripts.

Install the latest backup utility scripts from the tDAR Mercurial repository.

   ````
   mkdir -p ~/bin/tdarbackup
   cd ~/bin/tdarbackup
   hg clone https://bitbucket.org/tdar/backup ~/bin/tdarbackup
   ````


### Python PIP  & Required Python Libraries

> Make sure you log in as the the same user that will execute these scripts.

This section outlines the steps for installing *PIP*, *virtualenvwrapper*, and the 3rd-party python libraries required by the 
backup utility. 

1. Install *python3-pip*
   ````
   sudo apt-get install python3-pip
   ````

2. Install *virtualenvwrapper*  (more information available from http://virtualenvwrapper.readthedocs.org/en/latest/install.html#basic-installation)
   
   1. install the utility
   ````
   pip3 install virtualenvwrapper
   ````
   
   2. Add the following lines to the user startup file (e.g. ~/.bashrc) 
   ````
   export WORKON_HOME=$HOME/.virtualenvs
   export PROJECT_HOME=$HOME/Devel
   source /usr/local/bin/virtualenvwrapper.sh
   ````
   
3. Install required 3rd-party libraries
   ````
   mkvirtualenv tdarbackup
   workon tdarbackup
   pip3 install invoke==0.10.1
   pip3 install boto3==1.1.1
   ````

### Install Required Tools

#### GnuPG
The backup utility uses GnuPG to encrypt backups prior to sending/receiving to/from Glacier.  GnuPG is easy to install on most unix-based 
with package-management systems

To install on unix w/ apt-get:
````
sudo apt-get install gnupg
````

To install on OSX w/ brew:
````
brew install gnupg
````

#### S3CMD
Tdarbackup requires the latest s3cmd v1.6.1 or later.  This section describes how to perform the installation with  
_mkvirtualenv_ and _pip_, however, you can use a package manager instead so long as it installs v1.6.1 or higher.
    
1. create a new virtual environment that uses python v2 interpreter (will not work with python v3)
````
    mkvirtualenv -p python2 s3cmdenv
    workon s3cmdenv
    pip install s3cmd
````

2. Add the path to the s3cmd executable to your $PATH environment variable.  For example, in a bash environment:

export PATH=$PATH:~/.virtualenvs/s3cmdenv/bin/s3cmd


#### xxhash (optional)
_Note that xxhash is currently unused by tdarbackup. Feel free to skip this step._

Compile and install *xxhash* (more isntructions here https://github.com/Cyan4973/xxHash)

````
mkdir ~/xxhash
git clone https://github.com/Cyan4973/xxHash.git ~/xxhash
cd ~/xxhash
make
sudo cp xxhsum /usr/local/bin/xxhsum
sudo chmod +x /usr/local/bin/xxhsum

````


## Configuration

### Configure S3CMD

Launch the interactive configurator with the command `s3cmd --configure` 

Use the following settings: 

- Access Key: _not shown here_
- Secret Key: _not shown here_
- Default Region: US
- path to GPG: `/usr/local/bin/gpg` for OSX,   `/usr/bin/gpg` for Ubuntu
- Use HTTPS protocol: Yes
- Proxy Server: _leave blank_
- Test access with supplied credentials?:  Yes

If correctly configured, the configurator will state that the test was successful and that the access/secret key worked fine.  Disregard
the warning about encryption - the backup utility manages file encryption.  


### Configure the backup tool (config.ini)

The backup utility expects the config file to be located in  `~/.tdarbackup/config.ini`.  You can either
make a copy of `config.template.ini` which is located in the same folder as the utilty install folder,
or simply execute the following command:

    invoke prepare

After creating the config file, replace the default values for the following fields: 
  
  - aws_access_key_id
  - aws_secret_access_key
  - region_name

