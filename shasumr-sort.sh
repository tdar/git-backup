#!/bin/bash

#todo  http://stackoverflow.com/questions/2870992/automatic-exit-from-bash-shell-script-on-error

#run at lowest priority
#ionice -c3 -p$$ >/dev/null
#renice +15 -p $$ >/dev/null

thisdir="$(dirname $0)"
if [ $# -lt 2 ]; then
  echo "Usage: shasumr TARGETDIR OUTFILE" >@2
  exit 1
fi

mkdir -p /tmp/shasumr

targetdir=$1
outfile=$2
tmpfile=/tmp/shasumr/shasumr.$$.txt
datestart=$(date)

echo "compiling shasum for files..."
$thisdir/shasumr.sh "$targetdir" > $tmpfile
echo "sorting file listing..." 
sort $tmpfile > "$outfile"

dateend=$(date)
echo "COMPLETE"
echo "Start: $datestart"
echo "  End: $dateend"
